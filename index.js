const net = require('net');
const { spawn } = require('child_process');
const express = require('express')
const proxy = require('express-http-proxy');

const ports = [9500];

const ip = '192.168.150.5';
const pwd = '87cc9e0e36014698e08ecdfd25e4ceee';
const vlc_args = ['-I', 'rc', `rtsp://${ip}:554/${pwd}_0`, '--sout', '#std{access=livehttp{seglen=5,delsegs=true,numsegs=5,index=./public/stream.m3u8,index-url=./stream-########.ts},mux=ts{use-key-frames},dst=./public/stream-########.ts}']

function onData(data, socket) {
  if (data.toString().startsWith('heartbeat1')) {
    socket.write('{"msg_name":"Heartbeat","msg_index":2}')
    console.log('heartbeat')
    return
  }

  if (data.toString().startsWith('hopeway')) {
    console.log(data.toString('hex'));
    //socket.write('{"msg_index":23,"msg_name":"IpcBind","bind_flag":false,"user_key":"131610078685190339","ipc_id":"26300015641","msg_no":"5fee986f98be4","from":1485359}');
    socket.write('{"msg_index":23,"msg_name":"IpcBind","bind_flag":true,"user_key":"131610078685190339","ipc_id":"26300015641","msg_no":"5fee986f98be4","from":1485359}');

    /*
    const vlc_process = spawn('vlc-wrapper', vlc_args)
    vlc_process.stdout.on('data', (data) => {
      console.log('stdout:', data.toString());
    });
    vlc_process.stderr.on('data', (data) => {
      console.log('stderr:', data.toString());
    });
    vlc_process.on('close', (code) => {
      console.log(`vlc_process exited with code ${code}`);
    });
    */

    return;
  }

  // 0x7b == '{', the message is JSON
  if (data[0] == 0x7b) {
    console.log(JSON.parse(data));
  } else {
    console.log('onData', JSON.stringify(data.toString()))
  }
}

const servers = ports.map(port => {
  const server = net.createServer((socket) => {
    console.log('createServer', port);
    socket.on('connect', () => {
      console.log('connect', port)
    })

    socket.on('data', (data) => {
      onData(data, socket);
    })
  });

  server.listen(port);
  console.log('Listening on', port)
  return server
})


const app = express()
//app.use(express.urlencoded()); //Parse URL-encoded bodies
app.use(express.json()); //Used to parse JSON bodies
app.use(express.raw({type: 'application/x-www-form-urlencoded'}));

const apiIp = '47.254.78.198'
const apiHost = 'service.houwei-tech.com'
/*
app.use(proxy(apiIp, {
  proxyReqBodyDecorator: (bodyContent, srcReq) => {
    console.log('bodyContent', bodyContent.toString('hex'))
    return bodyContent;
  },
  proxyReqOptDecorator: (proxyReqOpts, srcReq) => {
    proxyReqOpts.headers['host'] = apiHost;
    proxyReqOpts.path = '/device/register';
    console.log(proxyReqOpts)
    return proxyReqOpts;
  },
  userResDecorator: (proxyRes, proxyResData, userReq, userRes) => {
    console.log('proxyResData', proxyResData.toString())
    return proxyResData;
  }
}));
*/

app.use(express.static('public'))

app.post('/device/register', (req, res) => {
  console.log('device/register', req.body.toString('hex'))

  res.send('{"ipc_datetime":"2021-01-02 03:53:27","msg_name":"IpcRegister","timezone":8,"cloud_url":"","cloud_package_index":0,"cloud_stream_type":1,"bcd_cloud":0,"cloud_md_always_record":true,"bind_flag":false,"user_key":"","pic_url":"http:\/\/service.houwei-tech.com\/device\/motionPic","ipc_uuid":"443649","ret":0,"ret_desc":"success","pull_url":"10.0.1.22:9500","verify":"5F13BF8B01FC64886C58643BDBEDD3EE","server_reserve":"3","upgrade_model":"","upgrade_version":"","upgrade_lfuv":"","upgrade_lfrc":"","upgrade_url":"","alarm_uri":"DEVICE_CALL_GATEWAY_HOST\/api\/DeviceAlarm\/alarmEventReport"}');
})

app.get('*', (req, res) => {
  console.log('request to', req.path)
});

app.listen(80, () => {
  console.log(`express listening`);
})

